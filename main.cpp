/*
 * A simple program to convert CSV and DATA files to XML files.
 */

#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <boost/algorithm/string.hpp>

using namespace std;
using namespace boost;

string getData(string fileName);

void convertCSVToXML(string data, string fileIndex);

void convertDATAToXML(string data, vector<string> headerFields, string fileIndex);

void convertToXML(stringstream &stream, vector<string> headerFields, string fileIndex);

void writeToFile(string data, string fileIndex);

const string CSVFilesPath = "/Users/Justas/CLionProjects/Projektai/ToXMLConverter/Data/CSV/";
const string DATAFilesPath = "//Users/Justas/CLionProjects/Projektai/ToXMLConverter/Data/DATA/";
const string resultPath = "/Users/Justas/CLionProjects/Projektai/ToXMLConverter/Result/";

int main() {
//    Begin of converting CSV files to XML files.

    convertCSVToXML(getData(CSVFilesPath + "1.csv"), "CSV_1");
    convertCSVToXML(getData(CSVFilesPath + "2.csv"), "CSV_2");
    convertCSVToXML(getData(CSVFilesPath + "3.csv"), "CSV_3");

//    End of converting CSV files to XML files.

//   Begin of converting DATA files to XML files.

    vector<string> headerFields = {"Sex", "Length", "Diameter", "Height", "Whole weight", "Shucked weight",
                                   "Viscera weight", "Shell weight", "Rings"};

    convertDATAToXML(getData(DATAFilesPath + "1.data"), headerFields, "DATA_1");

    headerFields.clear();
    headerFields = {"Age", "Workclass", "Fnlwgt", "Education", "Education-num", "Marital-status", "Occupation",
                    "Relationship", "Race", "Sex", "Capital-gain", "Capital-loss", "Hours-per-week",
                    "Native-country", "Prediction"};

    convertDATAToXML(getData(DATAFilesPath + "2.data"), headerFields, "DATA_2");

//    End of converting DATA files to XML files.

    return 0;
}

string getData(string fileName) {
    string data = "";

    ifstream fr(fileName);

    string line;
    while (getline(fr, line)) data += line + "\n";

    fr.close();

    return data;
}

void convertCSVToXML(string data, string fileIndex) {
    vector<string> headerFields;

    stringstream stream(data);

//    Begin of getting header fields.

    string header;
    while (getline(stream, header) && header.empty());

    stringstream headerStream(header);
    string headerField;
    while (getline(headerStream, headerField, ',')) {
        if (!headerField.empty() && headerField[headerField.length() - 1] == '\r')
            headerField.erase(headerField.length() - 1);

        headerFields.push_back(headerField);
    }

//    End of getting header fields.

    convertToXML(stream, headerFields, fileIndex);
}

void convertDATAToXML(string data, vector<string> headerFields, string fileIndex) {
    stringstream stream(data);

    convertToXML(stream, headerFields, fileIndex);
}

void convertToXML(stringstream &stream, vector<string> headerFields, string fileIndex) {
    string XML = "<root>\n";

    string line;
    while (getline(stream, line)) {
        string record;
        vector<string> fields;

        if (!line.empty()) {
            record = "<Record>\n";

            stringstream lineStream(line);

            string field;
            while (getline(lineStream, field, ',')) {
                if (!field.empty() && field[field.length() - 1] == '\r')
                    field.erase(field.length() - 1);

                fields.push_back(field);
            }

            for (int i = 0; i < fields.size(); i++) {
                if (!fields[i].empty()) {
                    string tag = i < headerFields.size() ? headerFields[i] : "Field";

                    trim(fields[i]);

                    record += '<' + tag + '>' + fields[i] + "</" + tag + ">\n";
                }
            }
            record += "</Record>\n\n";
            XML += record;
        }
    }
    XML += "</root>";

    writeToFile(XML, fileIndex);
}

void writeToFile(string data, string fileIndex) {
    ofstream fd(resultPath + fileIndex + ".xml");

    fd << data;

    fd.close();
}